<?php declare(strict_types=1);

defined('TYPO3_MODE') or die();

use TYPO3\CMS\Core\Core\Environment;

$context = Environment::getContext();

/**
 * Development environment
 * TYPO3_CONTEXT Development
 */
if ($context->isDevelopment()) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '../../.env.development');
    $dotenv->load();
    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '1';

    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '1';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = '12290';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[DEV] TYPO3 CMS';
}

/**
 * Staging environment
 * TYPO3_CONTEXT Production/Staging
 */
if ($context->isProduction() && $context->__toString()
    === 'Production/Staging') {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '../../.env.staging');
    $dotenv->load();

    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '';

    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = '4096';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[Staging] TYPO3 CMS';
}

/**
 * Staging environment
 * TYPO3_CONTEXT Production/Staging
 */
if ($context->isProduction() && $context->__toString() === 'Production') {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '../../.env');
    $dotenv->load();
    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '';

    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = '4096';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[Production] TYPO3 CMS';
}

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = $_ENV['DB_DB'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = $_ENV['DB_USER'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = $_ENV['DB_PASS'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = $_ENV['DB_HOST'];

$GLOBALS['TYPO3_CONF_VARS']['SYS']['folderCreateMask'] = '2770';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fileCreateMask'] = '0660';

