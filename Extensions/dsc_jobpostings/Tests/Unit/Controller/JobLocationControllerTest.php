<?php
declare(strict_types=1);

namespace DSC\DscJobpostings\Tests\Unit\Controller;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Oliver Thiele <mailYYYY@oliver-thiele.de>
 */
class JobLocationControllerTest extends UnitTestCase
{
    /**
     * @var \DSC\DscJobpostings\Controller\JobLocationController
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\DSC\DscJobpostings\Controller\JobLocationController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllJobLocationsFromRepositoryAndAssignsThemToView()
    {
        $allJobLocations = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $jobLocationRepository = $this->getMockBuilder(\DSC\DscJobpostings\Domain\Repository\JobLocationRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $jobLocationRepository->expects(self::once())->method('findAll')->will(self::returnValue($allJobLocations));
        $this->inject($this->subject, 'jobLocationRepository', $jobLocationRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('jobLocations', $allJobLocations);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenJobLocationToView()
    {
        $jobLocation = new \DSC\DscJobpostings\Domain\Model\JobLocation();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('jobLocation', $jobLocation);

        $this->subject->showAction($jobLocation);
    }
}
