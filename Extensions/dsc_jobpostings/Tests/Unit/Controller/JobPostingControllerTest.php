<?php
declare(strict_types=1);

namespace DSC\DscJobpostings\Tests\Unit\Controller;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Oliver Thiele <mailYYYY@oliver-thiele.de>
 */
class JobPostingControllerTest extends UnitTestCase
{
    /**
     * @var \DSC\DscJobpostings\Controller\JobPostingController
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\DSC\DscJobpostings\Controller\JobPostingController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllJobPostingsFromRepositoryAndAssignsThemToView()
    {
        $allJobPostings = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $jobPostingRepository = $this->getMockBuilder(\DSC\DscJobpostings\Domain\Repository\JobPostingRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $jobPostingRepository->expects(self::once())->method('findAll')->will(self::returnValue($allJobPostings));
        $this->inject($this->subject, 'jobPostingRepository', $jobPostingRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('jobPostings', $allJobPostings);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenJobPostingToView()
    {
        $jobPosting = new \DSC\DscJobpostings\Domain\Model\JobPosting();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('jobPosting', $jobPosting);

        $this->subject->showAction($jobPosting);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenJobPostingToJobPostingRepository()
    {
        $jobPosting = new \DSC\DscJobpostings\Domain\Model\JobPosting();

        $jobPostingRepository = $this->getMockBuilder(\DSC\DscJobpostings\Domain\Repository\JobPostingRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $jobPostingRepository->expects(self::once())->method('add')->with($jobPosting);
        $this->inject($this->subject, 'jobPostingRepository', $jobPostingRepository);

        $this->subject->createAction($jobPosting);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenJobPostingToView()
    {
        $jobPosting = new \DSC\DscJobpostings\Domain\Model\JobPosting();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('jobPosting', $jobPosting);

        $this->subject->editAction($jobPosting);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenJobPostingInJobPostingRepository()
    {
        $jobPosting = new \DSC\DscJobpostings\Domain\Model\JobPosting();

        $jobPostingRepository = $this->getMockBuilder(\DSC\DscJobpostings\Domain\Repository\JobPostingRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $jobPostingRepository->expects(self::once())->method('update')->with($jobPosting);
        $this->inject($this->subject, 'jobPostingRepository', $jobPostingRepository);

        $this->subject->updateAction($jobPosting);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenJobPostingFromJobPostingRepository()
    {
        $jobPosting = new \DSC\DscJobpostings\Domain\Model\JobPosting();

        $jobPostingRepository = $this->getMockBuilder(\DSC\DscJobpostings\Domain\Repository\JobPostingRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $jobPostingRepository->expects(self::once())->method('remove')->with($jobPosting);
        $this->inject($this->subject, 'jobPostingRepository', $jobPostingRepository);

        $this->subject->deleteAction($jobPosting);
    }
}
