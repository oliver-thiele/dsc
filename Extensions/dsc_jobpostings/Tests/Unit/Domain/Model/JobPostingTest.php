<?php
declare(strict_types=1);

namespace DSC\DscJobpostings\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Oliver Thiele <mailYYYY@oliver-thiele.de>
 */
class JobPostingTest extends UnitTestCase
{
    /**
     * @var \DSC\DscJobpostings\Domain\Model\JobPosting
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \DSC\DscJobpostings\Domain\Model\JobPosting();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDatePostedReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDatePosted()
        );
    }

    /**
     * @test
     */
    public function setDatePostedForDateTimeSetsDatePosted()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDatePosted($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'datePosted',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getValidThroughReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getValidThrough()
        );
    }

    /**
     * @test
     */
    public function setValidThroughForDateTimeSetsValidThrough()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setValidThrough($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'validThrough',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFilesReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFiles()
        );
    }

    /**
     * @test
     */
    public function setFilesForFileReferenceSetsFiles()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFiles->attach($file);
        $this->subject->setFiles($objectStorageHoldingExactlyOneFiles);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneFiles,
            'files',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFileToObjectStorageHoldingFiles()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $filesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $filesObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($file));
        $this->inject($this->subject, 'files', $filesObjectStorageMock);

        $this->subject->addFile($file);
    }

    /**
     * @test
     */
    public function removeFileFromObjectStorageHoldingFiles()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $filesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $filesObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($file));
        $this->inject($this->subject, 'files', $filesObjectStorageMock);

        $this->subject->removeFile($file);
    }

    /**
     * @test
     */
    public function getEmploymentTypeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getEmploymentType()
        );
    }

    /**
     * @test
     */
    public function setEmploymentTypeForIntSetsEmploymentType()
    {
        $this->subject->setEmploymentType(12);

        self::assertAttributeEquals(
            12,
            'employmentType',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJobLocationReturnsInitialValueForJobLocation()
    {
        self::assertEquals(
            null,
            $this->subject->getJobLocation()
        );
    }

    /**
     * @test
     */
    public function setJobLocationForJobLocationSetsJobLocation()
    {
        $jobLocationFixture = new \DSC\DscJobpostings\Domain\Model\JobLocation();
        $this->subject->setJobLocation($jobLocationFixture);

        self::assertAttributeEquals(
            $jobLocationFixture,
            'jobLocation',
            $this->subject
        );
    }
}
