<?php
declare(strict_types=1);

namespace DSC\DscJobpostings\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Oliver Thiele <mailYYYY@oliver-thiele.de>
 */
class JobLocationTest extends UnitTestCase
{
    /**
     * @var \DSC\DscJobpostings\Domain\Model\JobLocation
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \DSC\DscJobpostings\Domain\Model\JobLocation();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStreetAddressReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStreetAddress()
        );
    }

    /**
     * @test
     */
    public function setStreetAddressForStringSetsStreetAddress()
    {
        $this->subject->setStreetAddress('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'streetAddress',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAddressLocalityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAddressLocality()
        );
    }

    /**
     * @test
     */
    public function setAddressLocalityForStringSetsAddressLocality()
    {
        $this->subject->setAddressLocality('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'addressLocality',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAddressRegionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAddressRegion()
        );
    }

    /**
     * @test
     */
    public function setAddressRegionForStringSetsAddressRegion()
    {
        $this->subject->setAddressRegion('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'addressRegion',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPostalCodeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPostalCode()
        );
    }

    /**
     * @test
     */
    public function setPostalCodeForStringSetsPostalCode()
    {
        $this->subject->setPostalCode('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'postalCode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAddressCountryReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAddressCountry()
        );
    }

    /**
     * @test
     */
    public function setAddressCountryForStringSetsAddressCountry()
    {
        $this->subject->setAddressCountry('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'addressCountry',
            $this->subject
        );
    }
}
