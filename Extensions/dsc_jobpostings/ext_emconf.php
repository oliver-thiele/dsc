<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "dsc_jobpostings"
 *
 * Auto generated by Extension Builder 2021-06-23
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Job Postings',
    'description' => 'Job postings with structured data',
    'category' => 'plugin',
    'author' => 'Oliver Thiele',
    'author_email' => 'mailYYYY@oliver-thiele.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
