<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_dscjobpostings_domain_model_jobposting',
    'EXT:dsc_jobpostings/Resources/Private/Language/locallang_csh_tx_dscjobpostings_domain_model_jobposting.xlf'
);
//ExtensionManagementUtility::allowTableOnStandardPages('tx_dscjobpostings_domain_model_jobposting');
