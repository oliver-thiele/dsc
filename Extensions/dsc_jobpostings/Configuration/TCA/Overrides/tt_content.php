<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3_MODE') || die();

ExtensionUtility::registerPlugin(
    'DscJobpostings',
    'List',
    'Job Listing'
);

ExtensionUtility::registerPlugin(
    'DscJobpostings',
    'Show',
    'Single Job Posting'
);
