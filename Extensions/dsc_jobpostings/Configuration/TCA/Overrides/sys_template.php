<?php

defined('TYPO3_MODE') || die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
    'dsc_jobpostings',
    'Configuration/TypoScript',
    'Job Postings'
);
