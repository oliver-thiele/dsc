<?php

defined('TYPO3_MODE') || die();

call_user_func(
    static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'DscJobpostings',
        'List',
        [
            \DSC\DscJobpostings\Controller\JobPostingController::class => 'list, new, create, listJSON'
        ],
        // non-cacheable actions
        [
            \DSC\DscJobpostings\Controller\JobPostingController::class => 'create, update, delete',
            \DSC\DscJobpostings\Controller\JobLocationController::class => ''
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'DscJobpostings',
        'Show',
        [
            \DSC\DscJobpostings\Controller\JobPostingController::class => 'show'
        ],
        // non-cacheable actions
        [
            \DSC\DscJobpostings\Controller\JobPostingController::class => 'create, update, delete',
            \DSC\DscJobpostings\Controller\JobLocationController::class => ''
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    list {
                        iconIdentifier = dsc_jobpostings-plugin-list
                        title = LLL:EXT:dsc_jobpostings/Resources/Private/Language/locallang_db.xlf:tx_dsc_jobpostings_list.name
                        description = LLL:EXT:dsc_jobpostings/Resources/Private/Language/locallang_db.xlf:tx_dsc_jobpostings_list.description
                        tt_content_defValues {
                            CType = list
                            list_type = dscjobpostings_list
                        }
                    }
                    show {
                        iconIdentifier = dsc_jobpostings-plugin-show
                        title = LLL:EXT:dsc_jobpostings/Resources/Private/Language/locallang_db.xlf:tx_dsc_jobpostings_show.name
                        description = LLL:EXT:dsc_jobpostings/Resources/Private/Language/locallang_db.xlf:tx_dsc_jobpostings_show.description
                        tt_content_defValues {
                            CType = list
                            list_type = dscjobpostings_show
                        }
                    }
                }
                show = *
            }
       }'
    );

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'dsc_jobpostings-plugin-list',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:dsc_jobpostings/Resources/Public/Icons/user_plugin_list.svg']
    );
    $iconRegistry->registerIcon(
        'dsc_jobpostings-plugin-show',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:dsc_jobpostings/Resources/Public/Icons/user_plugin_show.svg']
    );
});
