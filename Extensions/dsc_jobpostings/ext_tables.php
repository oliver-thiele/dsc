<?php
defined('TYPO3_MODE') || die();

call_user_func(static function() {
//    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
//        'DscJobpostings',
//        'web',
//        'mod1',
//        '',
//        [
//            \DSC\DscJobpostings\Controller\JobPostingController::class => 'index, list, show, new, create, edit, update, delete',
//
//        ],
//        [
//            'access' => 'user,group',
//            'icon'   => 'EXT:dsc_jobpostings/Resources/Public/Icons/user_mod_mod1.svg',
//            'labels' => 'LLL:EXT:dsc_jobpostings/Resources/Private/Language/locallang_mod1.xlf',
//        ]
//    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'tx_dscjobpostings_domain_model_joblocation',
        'EXT:dsc_jobpostings/Resources/Private/Language/locallang_csh_tx_dscjobpostings_domain_model_joblocation.xlf'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'tx_dscjobpostings_domain_model_jobposting',
        'EXT:dsc_jobpostings/Resources/Private/Language/locallang_csh_tx_dscjobpostings_domain_model_jobposting.xlf'
    );

});
