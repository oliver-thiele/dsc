<?php

namespace DSC\DscJobpostings\View\JobPosting;


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class ListJSON
 * @package DSC\DscJobpostings\View\JobPostings
 */
class ListJSON extends \TYPO3\CMS\Extbase\Mvc\View\AbstractView
{
    /**
     * @return false|string
     */
    public function render()
    {
        // use TYPO3\CMS\Core\Utility\GeneralUtility;
// use TYPO3\CMS\Core\Database\ConnectionPool;
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
            'tx_dscjobpostings_domain_model_jobposting'
        );
        $statement = $queryBuilder
            ->select('uid', 'title', 'description')
            ->from('tx_dscjobpostings_domain_model_jobposting')
//            ->where(
//                $queryBuilder->expr()->eq('bodytext', $queryBuilder->createNamedParameter('klaus'))
//            )
            ->execute();
        while ($row = $statement->fetch()) {
            $jobPostings[$row['uid']] = $row;
//            DebuggerUtility::var_dump($row);
        }

//        $list = $this->viewData['jobPostings'];
//        DebuggerUtility::var_dump($this, __METHOD__);
//        $list = [
//            'A' => 1,
//            'B' => 2
//        ];
        return json_encode($jobPostings);
    }
}
