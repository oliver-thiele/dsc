<?php

declare(strict_types=1);

namespace DSC\DscJobpostings\Controller;


use DSC\DscJobpostings\Domain\Model\JobPosting;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * This file is part of the "Job Postings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Oliver Thiele <mailYYYY@oliver-thiele.de>, Web Development Oliver Thiele
 */

/**
 * JobPostingController
 */
class JobPostingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @param  \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface  $persistenceManager
     */
    public function injectPersistenceManager(PersistenceManagerInterface $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * jobPostingRepository
     *
     * @var \DSC\DscJobpostings\Domain\Repository\JobPostingRepository
     */
    protected $jobPostingRepository = null;

    /**
     * @param  \DSC\DscJobpostings\Domain\Repository\JobPostingRepository  $jobPostingRepository
     */
    public function injectJobPostingRepository(
        \DSC\DscJobpostings\Domain\Repository\JobPostingRepository $jobPostingRepository
    ) {
        $this->jobPostingRepository = $jobPostingRepository;
    }

    /**
     * jobLocationRepository
     *
     * @var \DSC\DscJobpostings\Domain\Repository\JobLocationRepository
     */
    protected $jobLocationRepository = null;

    /**
     * @param  \DSC\DscJobpostings\Domain\Repository\JobLocationRepository  $jobLocationRepository
     */
    public function injectJobLocationRepository(
        \DSC\DscJobpostings\Domain\Repository\JobLocationRepository $jobLocationRepository
    ) {
        $this->jobLocationRepository = $jobLocationRepository;
    }

    /**
     * action index
     *
     * @return string|object|null|void
     */
    public function indexAction()
    {
    }

    /**
     * action list
     *
     * @return string|object|null|void
     */
    public function listAction()
    {
//        $now = new \DateTime();
//        $tstamp = $now->format('U');
//        $jobPosting = new JobPosting();
//        $jobPosting->setTitle('Test '.$now->format('d.m.y H:i:s'));
//        $jobPosting->setDatePosted($now);
//        $jobPosting->setValidThrough($now);
//        $jobPosting->setEmploymentType(1);
//        $jobPosting->setDescription('foo bar');
//        $jobPosting->setPathSegment('test_'.$tstamp);
//        $jobLocation = $this->jobLocationRepository->findByUid(1);
//        $jobPosting->setJobLocation($jobLocation);
//
//        DebuggerUtility::var_dump($jobPosting, 'JP 1');
//        DebuggerUtility::var_dump($jobPosting->_isDirty(), 'Dirty?');
//        DebuggerUtility::var_dump($jobPosting->_isNew(), 'New?');
//
//
//        $this->addFlashMessage(
//            'The object was created.
//        Please be aware that this action is publicly accessible unless you implement an access check.
//         See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html',
//            '',
//            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
//        );
//        $this->jobPostingRepository->add($jobPosting);
//        DebuggerUtility::var_dump($jobPosting, 'JP 2');
//
//        $this->persistenceManager->persistAll();
//
//
//        DebuggerUtility::var_dump($jobPosting, 'JP 3');
//        DebuggerUtility::var_dump($this, __METHOD__);
//        DebuggerUtility::var_dump($GLOBALS['TSFE'], 'TSFE');

        $jobPostings = $this->jobPostingRepository->findAll();
//        DebuggerUtility::var_dump($this->settings, 'Settings ' . __METHOD__, );
//        DebuggerUtility::var_dump($jobPostings, __METHOD__);
        $this->view->assign('jobPostings', $jobPostings);

//        $html = $this->view->render();
//        echo $html;
//        die();
//        return $this->view->render();
    }


    public function initializeAction()
    {
//        DebuggerUtility::var_dump($this->request, 'Request in '. __METHOD__);
    }

    public function initializeShowAction()
    {
//        DebuggerUtility::var_dump($this->request, 'Request in '. __METHOD__);
        $this->request->setArgument('jobPosting', '1');
    }


    /**
     * action show
     *
     * @param  \DSC\DscJobpostings\Domain\Model\JobPosting  $jobPosting
     * @return string|object|null|void
     */
    public function showAction(\DSC\DscJobpostings\Domain\Model\JobPosting $jobPosting)
    {
//        $jopPosting = $this->jobPostingRepository->findByUid((int) $this->settings['list']['singlePid']);
//        DebuggerUtility::var_dump($this->request, 'Request');
//        DebuggerUtility::var_dump($jobPosting, __METHOD__);
        $this->view->assign('jobPosting', $jobPosting);
    }

    /**
     * action new
     *
     * @return string|object|null|void
     */
    public function newAction()
    {
    }


    /**
     * initialize create action
     *
     * @return void
     */
    public function initializeCreateAction()
    {
        $dateFormat = 'Y-m-d';
//            DebuggerUtility::var_dump($this->arguments);
        if ($this->arguments->hasArgument('newJobPosting')) {
            $this->arguments->getArgument('newJobPosting')->getPropertyMappingConfiguration()->forProperty(
                'datePosted'
            )->setTypeConverterOption(
                'TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter',
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                $dateFormat
            );
        }
        if ($this->arguments->hasArgument('newJobPosting')) {
            $this->arguments->getArgument('newJobPosting')->getPropertyMappingConfiguration()->forProperty(
                'validThrough'
            )->setTypeConverterOption(
                'TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter',
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                $dateFormat
            );
        }
    }

    /**
     * action create
     *
     * @param  \DSC\DscJobpostings\Domain\Model\JobPosting  $newJobPosting
     * @return string|object|null|void
     */
    public function createAction(\DSC\DscJobpostings\Domain\Model\JobPosting $newJobPosting)
    {
        $this->addFlashMessage(
            'The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->jobPostingRepository->add($newJobPosting);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param  \DSC\DscJobpostings\Domain\Model\JobPosting  $jobPosting
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("jobPosting")
     * @return string|object|null|void
     */
    public function editAction(\DSC\DscJobpostings\Domain\Model\JobPosting $jobPosting)
    {
        $this->view->assign('jobPosting', $jobPosting);
    }

    /**
     * action update
     *
     * @param  \DSC\DscJobpostings\Domain\Model\JobPosting  $jobPosting
     * @return string|object|null|void
     */
    public function updateAction(\DSC\DscJobpostings\Domain\Model\JobPosting $jobPosting)
    {
        $this->addFlashMessage(
            'The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->jobPostingRepository->update($jobPosting);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param  \DSC\DscJobpostings\Domain\Model\JobPosting  $jobPosting
     * @return string|object|null|void
     */
    public function deleteAction(\DSC\DscJobpostings\Domain\Model\JobPosting $jobPosting)
    {
        $this->addFlashMessage(
            'The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html',
            '',
            \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
        );
        $this->jobPostingRepository->remove($jobPosting);
        $this->redirect('list');
    }

    public function initializelistJSONAction(){
        $this->defaultViewObjectName = 'DSC\DscJobpostings\View\JobPosting\ListJSON';
    }

    /**
     *
     */
    public function listJSONAction()
    {
        $this->view->assign('arguments', $this->request->getArguments());
    }
}
