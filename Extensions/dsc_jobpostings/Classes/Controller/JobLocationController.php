<?php

declare(strict_types=1);

namespace DSC\DscJobpostings\Controller;


/**
 * This file is part of the "Job Postings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Oliver Thiele <mailYYYY@oliver-thiele.de>, Web Development Oliver Thiele
 */

/**
 * JobLocationController
 */
class JobLocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * jobLocationRepository
     *
     * @var \DSC\DscJobpostings\Domain\Repository\JobLocationRepository
     */
    protected $jobLocationRepository = null;

    /**
     * @param \DSC\DscJobpostings\Domain\Repository\JobLocationRepository $jobLocationRepository
     */
    public function injectJobLocationRepository(\DSC\DscJobpostings\Domain\Repository\JobLocationRepository $jobLocationRepository)
    {
        $this->jobLocationRepository = $jobLocationRepository;
    }

    /**
     * action list
     *
     * @return string|object|null|void
     */
    public function listAction()
    {
        $jobLocations = $this->jobLocationRepository->findAll();
        $this->view->assign('jobLocations', $jobLocations);
    }

    /**
     * action show
     *
     * @param \DSC\DscJobpostings\Domain\Model\JobLocation $jobLocation
     * @return string|object|null|void
     */
    public function showAction(\DSC\DscJobpostings\Domain\Model\JobLocation $jobLocation)
    {
        $this->view->assign('jobLocation', $jobLocation);
    }
}
