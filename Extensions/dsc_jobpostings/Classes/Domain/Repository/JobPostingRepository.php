<?php

declare(strict_types=1);

namespace DSC\DscJobpostings\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * This file is part of the "Job Postings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Oliver Thiele <mailYYYY@oliver-thiele.de>, Web Development Oliver Thiele
 */

/**
 * The repository for JobPostings
 */
class JobPostingRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function initializeObject() {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        // go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details

        // don't add the pid constraint
        $querySettings->setRespectStoragePage(FALSE);
        // set the storagePids to respect
//		$querySettings->setStoragePageIds(array(1, 26, 989));

        // don't add fields from enablecolumns constraint
        // this function is deprecated!
//		$querySettings->setRespectEnableFields(FALSE);

        // define the enablecolumn fields to be ignored
        // if nothing else is given, all enableFields are ignored
//		$querySettings->setIgnoreEnableFields(TRUE);
        // define single fields to be ignored
//		$querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime'));

        // add deleted rows to the result
//		$querySettings->setIncludeDeleted(TRUE);

        // don't add sys_language_uid constraint
//		$querySettings->setRespectSysLanguage(FALSE);

        // perform translation to dedicated language
//		$querySettings->setSysLanguageUid(42);
        $this->setDefaultQuerySettings($querySettings);
    }


    /**
     * Returns all objects of this repository.
     *
     * @return QueryResultInterface|array
     */
    public function findAll()
    {
        $query = $this->createQuery();


//        $query->equals('title12312', 'test');
        $queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);





//        DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL());
//        DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getParameters());

        return $query->execute();
    }
}
