<?php

declare(strict_types=1);

namespace DSC\DscJobpostings\Domain\Repository;


/**
 * This file is part of the "Job Postings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Oliver Thiele <mailYYYY@oliver-thiele.de>, Web Development Oliver Thiele
 */

/**
 * The repository for JobLocations
 */
class JobLocationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
