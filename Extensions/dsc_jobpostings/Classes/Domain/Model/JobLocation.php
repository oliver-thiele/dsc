<?php

declare(strict_types=1);

namespace DSC\DscJobpostings\Domain\Model;


/**
 * This file is part of the "Job Postings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Oliver Thiele <mailYYYY@oliver-thiele.de>, Web Development Oliver Thiele
 */

/**
 * Job Location
 */
class JobLocation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * Street Address
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $streetAddress = '';

    /**
     * Address Locality
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $addressLocality = '';

    /**
     * Address Region
     *
     * @var string
     */
    protected $addressRegion = '';

    /**
     * Postal Code
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $postalCode = '';

    /**
     * Address Country
     *
     * @var string
     */
    protected $addressCountry = '';

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Returns the streetAddress
     *
     * @return string $streetAddress
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Sets the streetAddress
     *
     * @param string $streetAddress
     * @return void
     */
    public function setStreetAddress(string $streetAddress)
    {
        $this->streetAddress = $streetAddress;
    }

    /**
     * Returns the addressLocality
     *
     * @return string $addressLocality
     */
    public function getAddressLocality()
    {
        return $this->addressLocality;
    }

    /**
     * Sets the addressLocality
     *
     * @param string $addressLocality
     * @return void
     */
    public function setAddressLocality(string $addressLocality)
    {
        $this->addressLocality = $addressLocality;
    }

    /**
     * Returns the addressRegion
     *
     * @return string $addressRegion
     */
    public function getAddressRegion()
    {
        return $this->addressRegion;
    }

    /**
     * Sets the addressRegion
     *
     * @param string $addressRegion
     * @return void
     */
    public function setAddressRegion(string $addressRegion)
    {
        $this->addressRegion = $addressRegion;
    }

    /**
     * Returns the postalCode
     *
     * @return string $postalCode
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Sets the postalCode
     *
     * @param string $postalCode
     * @return void
     */
    public function setPostalCode(string $postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * Returns the addressCountry
     *
     * @return string $addressCountry
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * Sets the addressCountry
     *
     * @param string $addressCountry
     * @return void
     */
    public function setAddressCountry(string $addressCountry)
    {
        $this->addressCountry = $addressCountry;
    }
}
