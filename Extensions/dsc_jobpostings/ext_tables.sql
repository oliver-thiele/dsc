CREATE TABLE tx_dscjobpostings_domain_model_jobposting (
	title varchar(255) NOT NULL DEFAULT '',
	date_posted datetime DEFAULT NULL,
	description text,
	valid_through datetime DEFAULT NULL,
	image int(11) unsigned NOT NULL DEFAULT '0',
	files int(11) unsigned NOT NULL DEFAULT '0',
	employment_type int(11) DEFAULT '0' NOT NULL,
	job_location int(11) unsigned DEFAULT '0',
	path_segment varchar(255) NOT NULL DEFAULT '',
);

CREATE TABLE tx_dscjobpostings_domain_model_joblocation (
	title varchar(255) NOT NULL DEFAULT '',
	street_address text NOT NULL DEFAULT '',
	address_locality varchar(255) NOT NULL DEFAULT '',
	address_region varchar(255) NOT NULL DEFAULT '',
	postal_code varchar(255) NOT NULL DEFAULT '',
	address_country varchar(255) NOT NULL DEFAULT ''
);
