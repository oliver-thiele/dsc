<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'CE Textmedia',
    'description' => 'Text and responsive image',
    'category' => 'frontend',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'author' => 'Oliver Thiele',
    'author_email' => 'mail@oliver-thiele.de',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.16-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
