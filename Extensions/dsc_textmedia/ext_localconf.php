<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Imaging\IconRegistry::class
        );

        // use same identifier as used in TSconfig for icon
        $iconRegistry->registerIcon(
        // use same identifier as used in TSconfig for icon
            'dsc-textmedia',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            // font-awesome identifier ('external-link-square')
            ['name' => 'star']
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '
        mod.wizards.newContentElement.wizardItems.common {
            elements {
                dsc_textmedia {
                    iconIdentifier = dsc-textmedia
                    title = LLL:EXT:dsc_textmedia/Resources/Private/Language/locallang_be.xlf:wizard.title
                    description = LLL:EXT:dsc_textmedia/Resources/Private/Language/locallang_be.xlf:wizard.description
                    tt_content_defValues {
                        CType = dsc_textmedia
                        imageorient = 26
                    }
                }
            }
            show := addToList(dsc_textmedia)
        }
        '
        );
    }
);
