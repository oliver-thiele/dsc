<?php

declare(strict_types=1);

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:dsc_textmedia/Resources/Private/Language/locallang_be.xlf:wizard.title',
        'dsc_textmedia',
        'dsc-textmedia',
    ],
    'textmedia',
    'after'
);
$GLOBALS['TCA']['tt_content']['types']['dsc_textmedia'] = [
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            --palette--;;headers,
            bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media, assets, imageorient,
            --palette--;;imagelinks,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
            --palette--;;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ',
    'columnsOverrides' => [
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
    ],
];


//
//'--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
//--palette--;;general,
//--palette--;;headers, bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
//--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media, assets,
//--palette--;;mediaAdjustments,
//--palette--;;gallerySettings,
//--palette--;;imagelinks,
//--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
//--palette--;;frames,
//--palette--;;appearanceLinks,
//--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
//--palette--;;language,
//--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
//--palette--;;hidden,
//--palette--;;access,
//--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
//--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category, categories,
//--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes, rowDescription,
//--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
